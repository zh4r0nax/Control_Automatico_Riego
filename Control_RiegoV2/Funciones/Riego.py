import time
import Config
import pymysql
import datetime
import logging
from logging.config import fileConfig
# import RPi.GPIO as GPIO
import os
import json

class Riego():
    #contructor
    def __init__(self):
        self.Titulo = 'Riego'
        self.sistemaOp = os.sys.platform
        self.Creacion = time.strftime("%d-%m-%y H:%M:%S")
        self.Host = Config.ConexionDB['Host']
        self.User = Config.ConexionDB['User']
        self.Pass = Config.ConexionDB['Pass']
        self.DBas = Config.ConexionDB['DBas']
        self.Pin = 38
        self.DBID = 0

        if(self.sistemaOp == 'win32'):
            logging.config.dictConfig(Config.LOGGING_CONTROL_AUTO_DEBUG_NT)
            
        elif(self.sistemaOp == 'linux'):
            logging.config.dictConfig(Config.LOGGING_CONTROL_AUTO_DEBUG_POSIX)

        self.RegistroLog('Debug', '__init__', str(self.sistemaOp)+' - ON')
    
    
    def run(self):
        try:
            self.RegistroLog('Debug', ' run ', ' Inicializado ')

            
            while True:
                self.ConfDB = self.BuscarConf()
                # self.DiaActual = time.strftime('%a')
                self.DiaActual = time.strftime('%A')
                
                #self.ConfGPIO()
                # bitencode=self.ConfDB[2].encode('hex')
                #Sun
                #Mon
                for i in range(len(self.ConfDB)):
                    if self.ConfDB[i] == "b'\\x01'":
                        self.ConfDB[i] = True
                    elif self.ConfDB[i] == "b'\\x00'":
                        self.ConfDB[i] = False
                    else:
                        pass
                

            
                #Lunes
                self.HoraActual = time.strftime('%H:%M:%S')

                if self.DiaActual == 'Monday':

                    if self.ConfDB[2] :
                        self.Regar()
                    else:
                        print(False)

                if self.DiaActual == 'Tuesday':

                    if self.ConfDB[3] :
                        self.Regar()
                    else:
                        print(False)

                if self.DiaActual == 'Wednesday':

                    if self.ConfDB[4] :
                        self.Regar()
                    else:
                        print(False)

                if self.DiaActual == 'Thursday':

                    if self.ConfDB[5] :
                        self.Regar()
                    else:
                        print(False)

                if self.DiaActual == 'Friday':

                    if self.ConfDB[6] :
                        self.Regar()
                    else:
                        print(False)

                if self.DiaActual == 'Saturday':

                    if self.ConfDB[7] :
                        self.Regar()
                    else:
                        print(False)

                if self.DiaActual == 'Sunday':

                    if self.ConfDB[8] :
                        self.Regar()
                    else:
                        print(False)


                    
                time.sleep(1)


        except KeyboardInterrupt:
            print('Saliendo')

        except Exception as identifier:
            print(identifier)
            self.RegistroLog('Error', ' run ', ' Exception -'+str(identifier))
            pass
        finally:
            print('Limpiando variables')
            time.sleep(1)
            print('Cerrando conexiones')
            time.sleep(1)
            pass
    
    def Regar(self):
        horaStart = self.ConfDB[0]
        horaStop = self.ConfDB[1]

        # self.ConfGPIOPin('Out',self.Pin)

        if self.HoraActual == horaStart:
            print('Start Riego')
            # self.SetPinTrue(self.Pin)
            #DB
            self.DBID = self.RegistroRiegoIni()

        elif self.HoraActual == horaStop:
            print('Stop Riego')
            # self.SetPinTrue(self.Pin)
            #DB
            self.RegistroRiegoFin(self.DBID)
        else:
            # print(' Regar - Sin Trabajo')
            pass


    def BuscarConf(self):
        # print('Buscando Config Riego')
        LstRestult = []
        db = pymysql.connect(self.Host,self.User,self.Pass,self.DBas)
        db.autocommit(True)
        cursor = db.cursor()
        query='call SP_Buscar_Conf(\''+self.Titulo+'\');'
        # print(query)
        cursor.execute(query)
        data = cursor.fetchone()
        

        strTiempo = str(datetime.timedelta(seconds=28800))
        for row in cursor._rows[0]:
            # print(str(row))
            LstRestult.append(str(row))



        cursor.close()
        db.close()
        return LstRestult

    def ConfGPIO(self):
        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BOARD)
        
    
    def ConfGPIOPin(self,Modo ,Pin):
        if(Modo == 'Out'):
            GPIO.setup(Pin, GPIO.OUT)
        elif(Modo == 'IN'):
            GPIO.setup(Pin, GPIO.IN)

    def SetPinTrue(sefl,Pin):
        GPIO.output(Pin, True)

    def SetPinFalse(sefl,Pin):
        GPIO.output(Pin, False)

    def Encender(self,Pin):
        print('Encendiendo Pin: '+str(Pin))
        #algforitmo de encender  pin

    def RegistroLog(self, Level, Metodo, Mensaje):
        if(self.sistemaOp == 'win32'):
            if(Level == 'Debug'):
                logging.debug('['+str(Metodo)+'] - '+str(Mensaje))
            elif(Level == 'Info'):
                logging.info('['+str(Metodo)+'] - '+str(Mensaje))
            elif(Level == 'Error'):
                logging.error('['+str(Metodo)+'] - '+str(Mensaje))
        elif(self.sistemaOp == 'linux'):
            if(Level == 'Debug'):
                logging.debug('\n['+str(Metodo)+'] - '+str(Mensaje))
            elif(Level == 'Info'):
                logging.info('\n['+str(Metodo)+'] - '+str(Mensaje))
            elif(Level == 'Error'):
                logging.error('\n['+str(Metodo)+'] - '+str(Mensaje))

        db = pymysql.connect(self.Host,self.User,self.Pass,self.DBas)
        db.autocommit(True)
        cursor = db.cursor()
        query='call SP_Insertar_Registro_Log(\''+str(Level)+'\',\''+str(Metodo)+'\',\''+str(Mensaje)+'\');'
        print(query)
        cursor.execute(query)
        data = cursor.fetchone()
        cursor.close()
        db.close()


    def RegistroRiegoIni(self):
        db = pymysql.connect(self.Host,self.User,self.Pass,self.DBas)
        db.autocommit(True)
        cursor = db.cursor()
        query = 'call SP_Registrar_Riego();'
        print(query)
        cursor.execute(query)
        data = cursor.fetchone()
        print('ID : %s' % data)
        print(data[0])
        
        cursor.close()
        db.close()
        return data[0]

    def RegistroRiegoFin(self,DBID):
        db = pymysql.connect(self.Host,self.User,self.Pass,self.DBas)
        db.autocommit(True)
        cursor = db.cursor()
        query = 'call SP_Registrar_Riego_Fin('+str(DBID)+');'
        print(query)
        cursor.execute(query)
        # data = cursor.fetchone()
        # print('ID : %s' % data)
        
        cursor.close()
        db.close()